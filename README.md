Temperature App using temperature data from OptumSoft socket server

Concerns:
   * Init doesn’t have the temperature sensor to which it belongs
   * Cannot set format of data i.e recent or minute
   * Mismatch of data points will throw an error which is observed in the console, this however doesn't affect functionality