// set up our data series with 150 random data points
var socket = io('http://interview.optumsoft.com');
var graph, seriesData = {}, seriesArray = [];

jQuery.get('http://interview.optumsoft.com/sensornames', function (names) {

    var palette = new Rickshaw.Color.Palette({ scheme: 'classic9' });

    for (var i = 0; i < names.length; i++) {
        seriesData[names[i]] = [];
        seriesArray.push({ color: palette.color(), data: seriesData[names[i]], name: names[i] });
    }

    for (i = 0; i < names.length; i++) {
        socket.emit("subscribe", names[i]);
    }

    socket.on("connect", function (data) {


        graph = new Rickshaw.Graph({
            element: document.getElementById("chart"),
            width: 900,
            height: 500,
            renderer: 'bar',
            stroke: true,
            preserve: true,
            series: seriesArray
        });

        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
            xFormatter: function (x) {
                return new Date(x * 1000).toString();
            }
        });

        var legend = new Rickshaw.Graph.Legend({
            graph: graph,
            element: document.getElementById('legend')

        });

        var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
            graph: graph,
            legend: legend
        });

        var order = new Rickshaw.Graph.Behavior.Series.Order({
            graph: graph,
            legend: legend
        });

        var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight({
            graph: graph,
            legend: legend
        });

        var smoother = new Rickshaw.Graph.Smoother({
            graph: graph,
            element: document.querySelector('#smoother')
        });

        var ticksTreatment = 'glow';

        var xAxis = new Rickshaw.Graph.Axis.Time({
            graph: graph,
            ticksTreatment: ticksTreatment,
            timeFixture: new Rickshaw.Fixtures.Time.Local()
        });

        xAxis.render();

        var yAxis = new Rickshaw.Graph.Axis.Y({
            graph: graph,
            tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
            ticksTreatment: ticksTreatment
        });

        yAxis.render();


        var controls = new RenderControls({
            element: document.querySelector('form'),
            graph: graph
        });

    });
    var temperatureKey = 0;

    socket.on("data", function (tempData) {

        if (tempData.type === 'init') {
            var initData = tempData.recent;
            for (var i = 0; i < initData.length; i++) {
                var obj = {};
                obj.x = initData[i].key;
                obj.y = initData[i].val;
                seriesData["temperature" + temperatureKey].push(obj);
            }
            temperatureKey++;
        }
        else if (tempData.type === 'update') {
            seriesData[tempData.sensor].push({ x: tempData.key, y: tempData.val });
            graph.update();
        }
        else if (tempData.type === 'delete') {
            var index = seriesData[tempData.sensor].findIndex((i) => i.x === tempData.key);
            if (index !== - 1)
                seriesData[tempData.sensor].splice(index, 1);
            graph.update();
        }
    });

});